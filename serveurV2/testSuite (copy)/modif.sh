#!/usr/bin/env bash

### FICHIERS REGULIERS ###
#modifier F1.TXT
touch F1.TXT
#supprimer F2.TXT
rm F2.TXT
#ne pas toucher F3.TXT

### DOSSIERS ###
#ne pas toucher D1
#supprimer D2
rmdir D2

### SYMLINKS ###
#ne pas toucher sl1
#changer pointeur sl2
ln -sfn ./F3.TXT sl2
#changer pointeur vers une cible invalide sl3
ln -sfn ./../../../../lienVersCibleInVaLiDe sl3
#suppression sl4
rm sl4
