#!/usr/bin/env bash
set -x
# startsurv start [-d x] fichier1 fichier2 ...
# démarre le serveur de surveillance de fichiers (fichier régulier, dossier, liens symboliques)
# par défaut, la surveillance des événements liés aux fichiers aura lieu toutes les 5 secondes.
# l’option -d x permet de fixer le nombre de secondes entre chaque inspection (par défaut toutes les
# 5 secondes).
#
# fichier1 fichier2 ... est la liste de tous les fichiers qui doivent être surveillés.
#
# startsurv stop
# stoppe le serveur.
# Tous les processus, fichiers et tubes impliqués doivent être correctement détruits.
#
# surveille restart
# stoppe le serveur et le rdémarre avec les mêmes paramètres initiaux. (15)

### INCLUSIONS ###
source activities.sh
### EXIT CODES ###
#   10   = Erreur au chargement de la configuration
#   20   = Erreur à l'exécution du script

### VARIABLES V2 ###
### VARIABLES ###
# Retrouve le chemin absolu vers le script en cours d'exécution
declare SCRIPT=$(readlink -f "$0")
# Extraction le chemin absolu du répertoire dans lequel se trouve le script en cours d'exécution
declare SCRIPTPATH="$(dirname "$SCRIPT")"
declare VERSION="1"
declare LOGFILE="startsurv.log"
declare freq="5"               # contient la fréquence passé en paramètre
declare -a files=()             # contient la liste des liens reçus en paramètre
declare -a REGULAR_FILE=() #contient les fichiers réguliers fournis en paramètre que l'on va surveiller
declare -a RF_LAST_MODIFICATION=()
declare -a RF_SIZE=()
declare -a RF_USED_BY=()
declare -a DIRECTORY=() #contient les répertoires fournis en paramètre que l'on va surveiller
declare -a D_EXISTS=()
declare -a SYMLINK=() #contient les liens symboliques fournis en paramètre que l'on va surveiller
declare -a SL_EXISTS=()
declare -a SL_TARGET=()
declare -a SL_INVALID=()


######################################################################################################"

##################################################################################################


### FUNCTION ###
function prepare_environment() {
    # Création des répertoires
    #mkdir -p $SCRIPTPATH/$VERSION
    # Création du fichier de log
    touch $SCRIPTPATH/$VERSION/$LOGFILE
    # Définition des droits de lecture/écriture pour l'utilisateur propriétaire uniquement
    chmod 600 $SCRIPTPATH/$VERSION/$LOGFILE
}


# Fonction de logging
# -------------------
# Envoie tous les arguments passés dans un fichier de log
#
# Usage: write_log "Message à logger"
#
function write_log() {
    # Inscrit la date, heure ainsi que le message complet dans le fichier de log
    echo "$(date +'%Y/%m/%d %H:%M:%S') ; $@" >> $SCRIPTPATH/$VERSION/$LOGFILE
    # Inscrit le message dans la console
    #echo -e "$@"
}

# Fonction de chargement des paramètres 
# --------------------------------------
# récupère les paramètres passés au lancement du script dans les variables PID, USERNAME, FREQ
#
# Usage : load_param $@
#
function start_debug() {
	prepare_symlinks
	while ( true ); 
	do 
		check_and_report_symlinks 
		sleep $freq 
	done	

}

function start() {
	prepare_regular_files
	#sleep 2
	prepare_directories
	#sleep 3
	prepare_symlinks
	#sleep 4
	while ( true ); do
        	check_and_report_regular_files
		#sleep 5
             	check_and_report_directories
		#sleep 6
             	check_and_report_symlinks
		sleep $freq
         done	
}

# Fonction de chargement des paramètres 
# --------------------------------------
# récupère les paramètres passés au lancement du script dans les variables PID, USERNAME, FREQ
#
# Usage : load_param $@
#

function load_params(){
	echo "load param $#"
	echo -e $1
	freq=$1
	shift
	while (( "$#" )); do
		store $1
		shift
	done
}

function store(){
	if [ ! -z "$1" ];
	then
		if [ -h "$1" ];
		then
			SYMLINK[${#SYMLINK[@]}]=$1
		elif [ -f "$1" ];
		then
			REGULAR_FILE[${#REGULAR_FILE[@]}]=$(realpath $1)
		elif [ -d "$1" ];
		then
			DIRECTORY[${#DIRECTORY[@]}]=$(realpath $1)
		else
			echo "le paramètre n'est pas un fichier, dossier ou symlink: $1"
		fi
	else
		echo "le paramètre s1 est vide val:<$1>"
	fi
}

### main logic ###
function main(){
	prepare_environment
	load_params "$@"
	start
}


### SCRIPT ###
main "$@"
