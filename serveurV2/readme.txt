la solution:

le serveur:
- startsurv (launcher du serveur en background)
- server.sh (effectue l'activité startsur demandé dans l'énoncé)
- activities.sh (contient les fonctions utilisées par server.sh)

ressources du serveur:
- ./NO_VERSION/startsurv.log (contient les activités détectée)
- ./NO_VERSION/startsurv.pid (contient le pid du processus exécutant server.sh)

le client [pas terminé]:
- reportevents (script permettant de lire l'activité serveur)

ressources du client:
- ./NO_VERSION/startsurv.cli (contient la date du dernier log lu)
