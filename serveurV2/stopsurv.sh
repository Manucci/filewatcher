#!/usr/bin/env bash
set -x

### variables ###
declare SCRIPTPATH="$(dirname "$SCRIPT")"
declare VERSION="1"
declare PIDFILE="startsurv.pid"

### functions ###

# Fonction de recherche et terminaison de processus 
# -------------------------------------------------
# Retrouve le pid du processus startsurv en cours d'exécution et vérifie que le processus est toujours en vie. Si c'est le cas, il va envoyer un signal pour terminer proprement son travail.
# Il y a une seconde vérification qui est faite et si le processus n'a pas terminé son travail il est tué brutalement. 
#
# Usage : retrieve_terminate_process
#
function retrieve_terminate_process(){
	echo "reference vers le fichier PID du serveur : $SCRIPTPATH/$VERSION/$PIDFILE"
	if [ -f $SCRIPTPATH/$VERSION/$PIDFILE ];
	then

		local pid=$(cat $SCRIPTPATH/$VERSION/$PIDFILE)
		echo "la valeur contenue dans le fichier startsurv.pid: $pid"
		if [[ ! -z ${pid} ]];
		then
			local psResult=$(ps --no-headers -p ${pid} | cut -d ' ' -f 1)
			echo "le résultat de [[ps --no-headers -p ${pid} | cut -d ' ' -f 1]]: $psResult"
			if [[ ! -z ${psResult} ]]; then
				echo "SIGTERM pour le process ${psResult}"
				sleep 5
				$(kill -SIGTERM ${psResult})
				local check=$(ps --no-headers -p ${psResult} | cut -d ' ' -f 1)
				echo "on verifie pour s'assurer que le processus est terminé: $check"
				if [[ ! -z ${check} ]]; then
					echo "SIGKILL pour le process ${check}"
					$(kill -SIGKILL ${check})
				fi
				delete_process_resources
			fi
		fi
	else
		echo "il n'y a pas de processus startsurv actif: le fichier pid n'existe pas"
	fi
}

# Fonction de suppression de ressources 
# -------------------------------------
# La fonction supprime le dossier ayant le numéro de version du serveur, lequel contient le fichier pid, log et cli. 
#
# Usage : delete_process_resources
#
function delete_process_resources(){
	$(rm -r -f $SCRIPTPATH/$VERSION)
}

### main logic ###
function main(){
	retrieve_terminate_process
}


### SCRIPT ###
main

