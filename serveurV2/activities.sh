#!/usr/bin/env bash
set -x

declare -a RF_EXISTS=()

# Fonction d'initialisation des fichiers reguliers 
# ------------------------------------------------
# Pour chaque fichier regulier, son existance, la date de derniere modification et sa taille est stockée en memoire
#
# Usage : prepare_regular_files
#
#si fichier existe alors
## stocker la date
## stocker stocker la taille
#sinon
# log: fichier n'existe pas
#finsi
function prepare_regular_files(){ 
	for ((i = 0; i < ${#REGULAR_FILE[@]}; i++)); do 
		if [[ -e ${REGULAR_FILE[$i]} ]];
		then
			RF_EXISTS[$i]=1
			#RF_LAST_MODIFICATION[$i]=$(date +%F' '%T -r ${REGULAR_FILE[$i]})
			RF_SIZE[$i]=$(stat -c%s ${REGULAR_FILE[$i]} | cut -d ' ' -f 1)
		else
			RF_EXISTS[$i]=0;
		fi
	done
}

# Fonction d'initialisation des répertoires
# -----------------------------------------
# Pour chaque fichier dossier, son existance est stockée en memoire
#
# Usage : prepare_directories
#
function prepare_directories(){ #todo factoriser la methode car elle peut serveur pour les symlinks et dossiers
	for ((i = 0; i < ${#DIRECTORY[@]}; i++)); do 
		if [[ -d ${DIRECTORY[$i]} ]];
		then
			D_EXISTS[$i]=1
		else
			D_EXISTS[$i]=0;
		fi
	done
}

# Fonction d'initialisation des liens symboliques 
# -----------------------------------------------
# Pour chaque lien symbolique est stocké en mémoire
# * son existance
# * sa référence
# * sa validité
#
# Usage : prepare_symlinks
#
function prepare_symlinks(){
	echo "prepare_symlinks()"
	for ((i = 0; i < ${#SYMLINK[@]}; i++)); do 
		if [[ -h ${SYMLINK[$i]} ]]; #a-t-on besoin de checker ?
		then
			SL_EXISTS[$i]=1
			local target=$(readlink -f ${SYMLINK[$i]})
			if [[ -e ${target} ]];
			then
				SL_TARGET[$i]=${target}	
				SL_INVALID[$i]=0
			else
				SL_INVALID[$i]=1
			fi
		else
			SL_EXISTS[$i]=0
			SL_INVALID[$i]=0
		fi
	done
}

# Fonction de vérification et de rapport des fichiers réguliers 
# -------------------------------------------------------------
# Pour chaque fichier régulier existant, sa date de dernière modification, sa taille est vérifiée, stockée si différente et rapportée
#
# Usage : check_and_report_regular_files
#
function check_and_report_regular_files(){
	for ((i = 0; i < ${#REGULAR_FILE[@]}; i++)); do
		if [ ${RF_EXISTS[$i]} -eq 1 ]; 
		then #si RF_EXISTS==0 alors il a déjà été reporté comme étant supprimé et on ne le traite plus
			if [ -e ${REGULAR_FILE[$i]} ];
			then #on vérifie s'il existe sinon on reporte l'evenement
				local newSize=$(stat -c%s ${REGULAR_FILE[$i]} | cut -d ' ' -f 1)
				if [ ${RF_SIZE[$i]} -ne ${newSize} ];
				then
					write_log "A1 ; ${REGULAR_FILE[$i]} ; modification de taille: ancienne taille ${RF_SIZE[$i]} nouvelle taille ${newSize}"
					RF_SIZE[$i]=${newSize}
				fi
				#lsof without error see => https://unix.stackexchange.com/questions/171519/lsof-warning-cant-stat-fuse-gvfsd-fuse-file-system
				#TODO log A5
			else
				write_log "A2 ; ${REGULAR_FILE[$i]} ; suppression du fichier"
				RF_EXISTS[$i]=0 #dorénavant il ne sera plus traité
			fi
		fi
	done
}


# Fonction de vérification et de rapport des dossiers 
# ---------------------------------------------------
# Pour chaque dossier, son existance est vérifée et rapportée
#
# Usage : check_and_report_directories
#
function check_and_report_directories(){
	for ((i = 0; i < ${#DIRECTORY[@]}; i++)); do 
		if [[ ${D_EXISTS[$i]} -eq 1 ]];
		then
			if [[ -d ${DIRECTORY[$i]} ]];
			then
				write_log "A2 ; ${DIRECTORY[$i]} ; suppression du dossier"
				D_EXISTS[$i]=0
			fi
			#TODO log A5
			#write_log "A5 ; ${DIRECTORY[$i]} ; utilisé par un processus PID=12345 - USER=Manucci - PROCESSNAME=todo"
		fi
	done
}

# Fonction de vérification et de rapport des liens symboliques 
# ------------------------------------------------------------
# Pour chaque lien symbolique est vérifiée et rapportée
# * son existance
# * son changement de référence
# * son invalidité
#
# Usage : check_and_report_symlinks
#
function check_and_report_symlinks(){
	echo "check_and_report_symlinks()"
	for ((i = 0; i < ${#SYMLINK[@]}; i++)); do 
		if [[ ${SL_EXISTS[$i]} == 1 ]];
		then
			#echo "sleep lancé, faut renommer le symlink"
			#sleep 10
			if [[ ! -h ${SYMLINK[$i]} ]];
			then
				write_log "A2 ; ${SYMLINK[$i]} ; suppression du lien symbolique"
				SL_EXISTS[$i]=0
			else
				#echo "sleep lancé, faut modifier la target"
				#sleep 10
				local target=$(readlink -f ${SYMLINK[$i]})
				if [[ ${target} != ${SL_TARGET[$i]} ]];
				then
					write_log "A3 ${SYMLINK[$i]} ; Changement de référence du pointage du lien symbolique: l'ancienne référence \'${SL_TARGET[$i]}\' la nouvelle référence \'${target}\'"
					SL_TARGET[$i]=${target}
				fi
			
				#on vérifie la validité du lien. Dans le cas où il devient invalide, on le loggue une seul fois. Par contre, on ne va pas s'arrêter de vérifier la cible.
				#contrairement au chemin du lien qui lui s'il devient invalide, on ne vérifiera plus.
				#echo "sleep lancé, faut rendre la target invalide"
				#sleep 10		
				if [[ ! -e $(readlink -f ${SYMLINK[$i]}) ]];
				then
					if [[ ${SL_INVALID[$i]} -eq 0 ]]; then
						write_log "A4 ; ${SYMLINK[$i]} ; Lien symbolique devenu invalide: \'${SYMLINK[$i]}\' -> \'${SL_TARGET[$i]}\'"
						SL_INVALID[$i]=1
					fi
				else
					SL_INVALID[$i]=0
				fi
			fi

		fi
	done
}
