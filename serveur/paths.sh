#!/usr/bin/env bash
set -x

declare abs_path_exists

# Fonction de vérification
# ------------------------------------------
# recoit un chemin absolu en paramètre et vérifie s'il existe
# usage: 
# 	abs_path_exists /home/manu/Documents
# 	abs_path_exists /home/manu/Documents/lib.sh
# 	
# résultat: -1 s'il n'existe pas, 0 s'il existe
# stock dans la variable 'abs_path_exists', le résultat entier
#
function abs_path_exists(){
	# Vérifie s'il y a au moins 1 paramètre
	if [ $# == 1 ]; then
		# Vérifie si le premier paramètre est un répertoire
		if [ -d "$1" ]; then
			# La variable globale "abs_path_exists" prend la valeur "0"
			abs_path_exists=0
		# Vérifie si le premier paramètre est un lien symbolique
		elif [ -L "$1" ]; then
			# La variable globale "abs_path_exists" prend la valeur "0"
			abs_path_exists=0
		# Vérifie si le premier paramètre est un fichier
		elif [ -f "$1" ]; then
			# La variable globale "abs_path_exists" prend la valeur "0"
			abs_path_exists=0
		# Si le premier paramètre n'est ni un répertoire, ni un lien symbolique, ni un fichier
		else
			# La variable globale "abs_path_exists" prend la valeur "-1"
			abs_path_exists=-1
		fi
	# S'il n'y a pas de paramètre
	else
		# La variable globale "abs_path_exists" prend la valeur "-1"
		abs_path_exists=-1
	fi 
}

declare rel_path_exists
# Fonction de vérification
# ------------------------------------------
# recoit un chemin relatif en paramètre et vérifie s'il existe
# usage: 
# 	rel_path_exists ./Documents
# 	rel_path_exists ./lib.sh
# 	
# résultat: -1 s'il n'existe pas, 0 s'il existe
# stock dans la variable 'rel_path_exists', le résultat entier
#
function rel_path_exists(){
#TODO: nettoyer le chemin de tout './' et '../'
	# Vérifie s'il y a au moins 1 paramètre
	if [ $# == 1 ]; then
		# XXX: need more info to comment
		abs_path_exists $(pwd)/$1
		rel_path_exists=$abs_path_exists
	else
		# La variable globale "rel_path_exists" prend la valeur "-1"
		rel_path_exists=-1
	fi
}

declare file_exists
# Fonction de vérification
# ------------------------------------------
# recoit un paramètre de type  soit fichier régulier, soit dossier soit simlink et vérifie s'il existe
# usage: 
# 	file_exists Documents
# 	file_exists lib.sh
# 	
# résultat: -1 s'il n'existe pas, 0 s'il existe
# stock dans la variable 'file_exists', le résultat entier
#
function file_exists(){
	# Vérifie s'il y a au moins 1 paramètre
	if [ $# == 1 ]; then
		# XXX: need more info to comment
		rel_path_exists $(pwd)/$1
		file_exists=$rel_path_exists
	else
		# La variable globale "file_exists" prend la valeur "-1"
		file_exists=-1
	fi
}	

declare absolute_path
# Fonction de conversion
# ------------------------------------------
# recoit un chemin ou un nom de fichier et le converti en chemin absolu
# usage: 
# 	convert_to_absolute_path /home/manu/Documents
# 	convert_to_absolute_path ./Documents
# 	convert_to_absolute_path Documents
# résultat: /home/manu/Documents
# stock dans la variable 'absolute_path', le chemin absolu
#
function convert_to_absolute_path(){
	#TODO: remplacer '\' par '/' avant de fournir l'argument au sous fonction
	#local absolute_path=""
	# Vérifie s'il y a au moins 1 paramètre
	if [ $# == 1 ]; then
		# Vérifie si le premier paramètre est égal à '/'
		if [[ $1 = /* ]]; then
			# Passer le premier paramètre à la fonction de vérification "abs_path_exists"
			abs_path_exists $1
			# Si la fonction "abs_path_exists" retourne le code 0
			if [[ $abs_path_exists == 0 ]]; then
				# Stocke le premier paramètre dans la variable absolute_path
				absolute_path=$1
			fi
		# Vérifie si le premier paramètre est différent de '/'
		elif [[ $1 != /* ]]; then
				# Passer le premier paramètre à la fonction de vérification "rel_path_exists"
	    		rel_path_exists $1
	    	# Si la fonction "rel_path_exists" retourne le code 0
			if [[ $rel_path_exists == 0 ]]; then
				# FIXME: découvrir le résultat
				absolute_path=$(pwd)/$1
			fi
		# Si le premier paramètre n'est ni '/' ni différent de '/' (?)
		# XXX: portion de code superflue ? nom de fichier et pas un chemin !
		else
			# Passer le premier paramètre à la fonction de vérification "file_exists"
	    	file_exists $1
	    	# Si la fonction "file_exists" retourne le code 0
			if [[ $file_exists == 0 ]]; then
				# FIXME: découvrir le résultat
				absolute_path=$(pwd)/$1
				echo $absolute_path
				sleep 10
			fi
		fi
	fi
}
