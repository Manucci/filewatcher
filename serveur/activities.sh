#!/usr/bin/env bash
set -x

# Fonction d'initialisation des fichiers reguliers 
# ------------------------------------------------
# Pour chaque fichier regulier, son existance, la date de derniere modification et sa taille est stockée en memoire
#
# Usage : prepare_regular_files
#
function prepare_regular_files(){ 
	for ((i = 0; i < ${#REGULAR_FILE[@]}; i++)); do 
		if [[ -e ${REGULAR_FILE[$i]} ]];
		then
			RF_EXISTS[$i]=1
			
			local lastTime=$(date +%F' '%T -r ${REGULAR_FILE[$i]})
			if [[ -n ${lastTime} ]];
			then
				RF_LAST_MODIFICATION[$i]=${lastTime}
			fi
			#TODO stocker la taille
			RF_SIZE[$i]=$(wc -c ${REGULAR_FILE[$i]} | cut -d ' ' -f 1)
			#TODO savoir si le fichier est utilisé par un processus
		else
			FR_EXISTS[$i]=0;
		fi
	done
}

# Fonction d'initialisation des répertoires
# -----------------------------------------
# Pour chaque fichier dossier, son existance est stockée en memoire
#
# Usage : prepare_directories
#
function prepare_directories(){ #todo factoriser la methode car elle peut serveur pour les symlinks et dossiers
	for ((i = 0; i < ${#DIRECTORY[@]}; i++)); do 
		if [[ -d ${DIRECTORY[$i]} ]];
		then
			D_EXISTS[$i]=1
		else
			D_EXISTS[$i]=0;
		fi
	done
}

# Fonction d'initialisation des liens symboliques 
# -----------------------------------------------
# Pour chaque lien symbolique est stocké en mémoire
# * son existance
# * sa référence
# * sa validité
#
# Usage : prepare_symlinks
#
function prepare_symlinks(){
	for ((i = 0; i < ${#SYMLINK[@]}; i++)); do 
		if [[ -h ${SYMLINK[$i]} ]];
		then
			SL_EXISTS[$i]=1
			SL_TARGET[$i]=$(readlink -f ${SYMLINK[$i]}) 
			if [ -f ${SL_TARGET[$i]} ]; 
			then
				SL_INVALID[$i]=1
			else
				SL_INVALID[$i]=0
			fi
		else
			SL_EXISTS[$i]=0
		fi
	done
}

# Fonction de vérification et de rapport des fichiers réguliers 
# -------------------------------------------------------------
# Pour chaque fichier régulier existant, sa date de dernière modification, sa taille est vérifiée, stockée si différente et rapportée
#
# Usage : check_and_report_regular_files
#
function check_and_report_regular_files(){
	for ((i = 0; i < ${#REGULAR_FILE[@]}; i++)); do 
		if [ ${RF_EXISTS[$i]} -eq 1 ] && [ -e ${REGULAR_FILE[$i]} ];
		then
			#retrouver la date de modification d'un fichier
			local lastTime=$(date +%F' '%T -r ${REGULAR_FILE[$i]})
			
			if [[ ${RF_LAST_MODIFICATION[$i]} != ${lastTime} ]];
			then
				local newSize=$(wc -c ${REGULAR_FILE[$i]} | cut -d ' ' -f 1)
				write_log "A1 ; ${REGULAR_FILE[$i]} ; Modiﬁcation de taille: ancienne taille ${RF_SIZE[$i]} la nouvelle taille ${newSize}"
				RF_SIZE[$i]=${newSize}
			fi
			RF_LAST_MODIFICATION[$i]=${lastTime}
		else
			RF_EXISTS[$i]=0
			write_log "A2 ; ${REGULAR_FILE[$i]} ; suppression "
		fi
	done
}

# Fonction de vérification et de rapport des dossiers 
# ---------------------------------------------------
# Pour chaque dossier, son existance est vérifée et rapportée
#
# Usage : check_and_report_directories
#
function check_and_report_directories(){
	for ((i = 0; i < ${#DIRECTORY[@]}; i++)); do 
		if [[ ! ${D_EXISTS[$i]} -eq 1 ]] || [[ ! -d ${DIRECTORY[$i]} ]];
		then
			D_EXISTS[$i]=0
			write_log "A2 ; ${DIRECTORY[$i]} ; suppression "
		fi
	done
}

# Fonction de vérification et de rapport des liens symboliques 
# ------------------------------------------------------------
# Pour chaque lien symbolique est vérifiée et rapportée
# * son existance
# * son changement de référence
# * son invalidité
#
# Usage : check_and_report_symlinks
#
function check_and_report_symlinks(){
	for ((i = 0; i < ${#SYMLINK[@]}; i++)); do 
		if [[ ${SL_EXISTS[$i]} == 1 ]] && [[ -h ${SYMLINK[$i]} ]];
		then
			SL_EXISTS[$i]=1
			local target=$(readlink -q ${SYMLINK[$i]})
			if [[ ${target} != ${SL_TARGET[$i]} ]];
			then
				SL_TARGET[$i]=${target}
				write_log "A3 ${SYMLINK[$i]} ; Changement de référence du pointage du lien symbolique: l'ancienne référence ${SL_TARGET[$i]} la nouvelle référence ${target}"
			fi
			
			if [[ ! -f ${SL_TARGET[$i]} ]];
			then
				SL_EXISTS[$i]=0
				write_log "A4 ; ${SYMLINK[$i]} ; Lien symbolique devenu invalide"
        	fi
		else
			SL_EXISTS[$i]=0
			write_log "A2 ; ${SYMLINK[$i]} ; suppression "
		fi
	done
}



