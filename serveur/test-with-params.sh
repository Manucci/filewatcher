#!/usr/bin/env bash

### VARIABLES ###
# Retrouve le chemin absolu vers le script en cours d'exécution
declare SCRIPT=$(readlink -f "$0")
# Extraction le chemin absolu du répertoire dans lequel se trouve le script en cours d'exécution
declare SCRIPTPATH="$(dirname "$SCRIPT")"
declare FILEPATH="/etc/bash.bashrc"
declare DIRECTORYPATH="/usr/local/bin"
declare SYMLINKPATH="/dev/cdrom"
declare FILELOG="$SCRIPTPATH/test-file.log"
declare DIRECTORYLOG="$SCRIPTPATH/test-directory.log"
declare SYMLINKLOG="$SCRIPTPATH/test-symlink.log"

### FONCTIONS ###
# Routine pour créer les fichiers de log et les
function prepare_environment {
    # Création des fichiers de log
    touch $FILELOG
    touch $DIRECTORYLOG
    touch $SYMLINKLOG
    # Définition des droits de lecture/écriture pour l'utilisateur propriétaire uniquement
    chmod 600 $FILELOG
    chmod 600 $DIRECTORYLOG
    chmod 600 $SYMLINKLOG
}

function test_a_file {
	# Test de la présence du fichier défini dans $FILEPATH
	if [ -f $FILEPATH ];
	then
		echo "this file exists"
	else
		echo "this file does not exist"
	fi
}

function test_a_directory {
	# Test de la présence du répertoire défini dans $DIRECTORYPATH
	if [ -d $DIRECTORYPATH ];
	then
		echo "this directory exists"
	else
		echo "this directory does not exist"
	fi
}

function test_a_symlink {
	# Test de la présence du lien symbolique défini dans $SYMLINKPATH
	if [ -h $SYMLINKPATH ];
	then
		echo "this symlink exists"
	else
		echo "this symlink does not exist"
	fi
}


### SCRIPT ###
prepare_environment
echo "====================================================="
echo "Log location = $SCRIPTPATH"
test_a_file > $FILELOG
test_a_directory > $DIRECTORYLOG
test_a_symlink > $SYMLINKLOG
echo "====================================================="
bash $1 $SCRIPTPATH $DIRECTORYPATH $SYMLINKPATH
echo "====================================================="
echo "Affichage des fichiers de log :"
cat $FILELOG
cat $DIRECTORYLOG
cat $SYMLINKLOG
echo "====================================================="


