#!/usr/bin/env bash
set -x

#Enﬁn, le script stopsurv est chargé de terminer les services du serveur. 
#Il veillera donc à ce que le processus serveur soit terminé et que les ﬁchiers temporaires(tubes,ﬁchiers,...) utilisés soient correctement détruits.
